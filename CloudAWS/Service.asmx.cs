﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Amazon.SQS;
using Amazon.SQS.Model;
using Amazon.Runtime;
using Amazon;
using System.Xml;

namespace CloudAWS
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://quamber.net")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        public static string Capitalize(string str)
        {
            string result = str;
            if (!string.IsNullOrEmpty(str))
            {
                var words = str.Split(' ');
                for (int index = 0; index < words.Length; index++)
                {
                    var s = words[index];
                    if (s.Length > 0)
                    {
                        words[index] = s[0].ToString().ToUpper() + s.Substring(1);
                    }
                }
                result = string.Join(" ", words);
            }
            return result;
        }
        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
        [WebMethod]
        public XmlDocument ProcessCall(String functionName = "toLowerCase", String data = "")
        {
            Amazon.Util.ProfileManager.RegisterProfile("QuamberAli", "AKIAICW2DJ6MJSQEXNPA", "hVq1DD9LmIXMSsdIsU3hu0e34f8A9y4NxAmNkaWy");

            AWSCredentials Credentials = new StoredProfileAWSCredentials("QuamberAli");

            var sqs = new AmazonSQSClient(Credentials, RegionEndpoint.USWest2);

            try
            {

                //Creating a queue
                var sqsRequest = new CreateQueueRequest { QueueName = "PungaInbox" };
                var createQueueResponse = sqs.CreateQueue(sqsRequest);
                string myQueueUrl = createQueueResponse.QueueUrl;


                ////Sending a message
                Dictionary<String, MessageAttributeValue> messageAttributesIB = new Dictionary<String, MessageAttributeValue>();

                MessageAttributeValue atrIB = new MessageAttributeValue();
                atrIB.StringValue = functionName;
                atrIB.DataType = "String";
                messageAttributesIB.Add("function", atrIB);
                var sendMessageRequest = new SendMessageRequest
                {
                    QueueUrl = myQueueUrl, //URL from initial queue creation
                    MessageBody = data,
                    MessageAttributes = messageAttributesIB

                };
                sqs.SendMessage(sendMessageRequest);

                //Receiving a message
                var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = myQueueUrl };
                receiveMessageRequest.MaxNumberOfMessages = 1;
                var receiveMessageResponse = sqs.ReceiveMessage(receiveMessageRequest);
                if (receiveMessageResponse.Messages != null)
                {
                     foreach (var message in receiveMessageResponse.Messages)
                    {
                        if (!string.IsNullOrEmpty(message.Body))
                        {
                            Amazon.Util.ProfileManager.RegisterProfile("QuamberAli", "AKIAICW2DJ6MJSQEXNPA", "hVq1DD9LmIXMSsdIsU3hu0e34f8A9y4NxAmNkaWy");

                            AWSCredentials CredentialsWB = new StoredProfileAWSCredentials("QuamberAli");

                            var sqsWB = new AmazonSQSClient(CredentialsWB, RegionEndpoint.USWest2);
                            var sqsRequestWB = new CreateQueueRequest { QueueName = "PungaOutbox" };
                            var createQueueResponseWB = sqs.CreateQueue(sqsRequestWB);
                            string myQueueUrlWB = createQueueResponseWB.QueueUrl;
                            String msgWB = message.Body;
                            if (functionName.Equals("toUpperCase"))
                            {
                                //msgWB = message.Body.Substring(message.Body.Split(' ')[0].Length, message.Body.Length);
                                msgWB = msgWB.ToUpper();


                            }
                            else if (functionName.Equals("toLowerCase"))
                            {
                                //msgWB = message.Body.Substring(message.Body.Split(' ')[0].Length, (message.Body.Length - message.Body.Split(' ')[0].Length));
                                msgWB = msgWB.ToLower();
                            }
                            else if (functionName.Equals("Reverse"))
                            {
                                //  msgWB = message.Body.Substring(message.Body.Split(' ')[0].Length, message.Body.Length);
                                msgWB = Reverse(msgWB);

                            }
                            else if (functionName.Equals("Capitalize"))
                            {
                                msgWB = Capitalize(msgWB);

                            }
                            var purgeOutbox = new PurgeQueueRequest { QueueUrl = myQueueUrlWB };
                            sqsWB.PurgeQueue(purgeOutbox);
                            Dictionary<String, MessageAttributeValue> messageAttributes = new Dictionary<String, MessageAttributeValue>();
                            MessageAttributeValue functionToBeCalled = new MessageAttributeValue();
                            functionToBeCalled.StringValue = functionName;

                            functionToBeCalled.DataType = "String";

                            messageAttributes.Add("function", functionToBeCalled);
                            MessageAttributeValue InboxQueueId = new MessageAttributeValue();
                            InboxQueueId.StringValue = message.MessageId;

                            InboxQueueId.DataType = "String";
                            messageAttributes.Add("InboxQueueId", InboxQueueId);

                            var sendMessageRequestWB = new SendMessageRequest
                            {
                                QueueUrl = myQueueUrlWB, //URL from initial queue creation

                                MessageBody = msgWB,
                                MessageAttributes = messageAttributes

                            };
                            SendMessageResponse responceWB = sqsWB.SendMessage(sendMessageRequestWB);
                            XmlDocument dom = new XmlDocument();

                            XmlElement messageTag = dom.CreateElement("Message");
                            dom.AppendChild(messageTag);

                            XmlElement MessageMD5 = dom.CreateElement("MessageMD5");
                            messageTag.AppendChild(MessageMD5);
                            XmlText textmd5 = dom.CreateTextNode(responceWB.MD5OfMessageBody);
                            MessageMD5.AppendChild(textmd5);
                            XmlElement MessageID = dom.CreateElement("Identifier");
                            messageTag.AppendChild(MessageID);
                            XmlText textID = dom.CreateTextNode(responceWB.MessageId);
                            MessageID.AppendChild(textID);
                            XmlElement MessageString = dom.CreateElement("String");
                            messageTag.AppendChild(MessageString);
                            XmlText textString = dom.CreateTextNode(msgWB);
                            MessageString.AppendChild(textString);
                            return dom;
                        }
                        Console.WriteLine("Deleting the message.\n");
                        var messageRecieptHandle = message.ReceiptHandle;

                        var deleteRequest = new DeleteMessageRequest { QueueUrl = myQueueUrl, ReceiptHandle = messageRecieptHandle };
                        sqs.DeleteMessage(deleteRequest);
                         
                        //foreach (string attributeKey in message.Attributes.Keys)
                        //{
                        //    Console.WriteLine("  Attribute");
                        //    Console.WriteLine("    Name: {0}", attributeKey);
                        //    var value = message.Attributes[attributeKey];
                        //    Console.WriteLine("    Value: {0}", string.IsNullOrEmpty(value) ? "(no value)" : value);
                        //}
                    }

                    //var messageRecieptHandleWB = receiveMessageResponse.Messages[0].ReceiptHandle;

                    ////Deleting a message
                    //Console.WriteLine("Deleting the message.\n");
                    //var deleteRequest = new DeleteMessageRequest { QueueUrl = myQueueUrl, ReceiptHandle = messageRecieptHandle };
                    //sqs.DeleteMessage(deleteRequest);
                }

            }
            catch (AmazonSQSException ex)
            {
                Console.WriteLine("Caught Exception: " + ex.Message);
                Console.WriteLine("Response Status Code: " + ex.StatusCode);
                Console.WriteLine("Error Code: " + ex.ErrorCode);
                Console.WriteLine("Error Type: " + ex.ErrorType);
                Console.WriteLine("Request ID: " + ex.RequestId);
                XmlDocument dom = new XmlDocument();

                XmlElement messageTag = dom.CreateElement("Error");
                dom.AppendChild(messageTag);

                XmlElement MessageMD5 = dom.CreateElement("RequestID");
                messageTag.AppendChild(MessageMD5);
                XmlText textmd5 = dom.CreateTextNode(ex.RequestId);
                MessageMD5.AppendChild(textmd5);
                XmlElement MessageID = dom.CreateElement("Exception");
                messageTag.AppendChild(MessageID);
                XmlText textID = dom.CreateTextNode(ex.Message);
                MessageID.AppendChild(textID);
                return dom;
            }

            XmlDocument dom2 = new XmlDocument();

            XmlElement messageTag2 = dom2.CreateElement("Error");
            dom2.AppendChild(messageTag2);

            XmlElement Message = dom2.CreateElement("Message");
            messageTag2.AppendChild(Message);
            XmlText textmd = dom2.CreateTextNode("Unable to Process Request");
            Message.AppendChild(textmd);
            return dom2;
        }
        [WebMethod]
        public XmlDocument OutboxMessageRead(String messageID)
        {
            try
            {

                Amazon.Util.ProfileManager.RegisterProfile("QuamberAli", "AKIAICW2DJ6MJSQEXNPA", "hVq1DD9LmIXMSsdIsU3hu0e34f8A9y4NxAmNkaWy");

                AWSCredentials CredentialsWB = new StoredProfileAWSCredentials("QuamberAli");
                String outputID = "";
                var sqsWB = new AmazonSQSClient(CredentialsWB, RegionEndpoint.USWest2);
                var sqsRequestWB = new CreateQueueRequest { QueueName = "PungaOutbox" };
                var createQueueResponseWB = sqsWB.CreateQueue(sqsRequestWB);
                string myQueueUrlWB = createQueueResponseWB.QueueUrl;
                var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = myQueueUrlWB };
                receiveMessageRequest.MaxNumberOfMessages = 1;
                var receiveMessageResponse = sqsWB.ReceiveMessage(receiveMessageRequest);

                if (receiveMessageResponse.Messages != null)
                {
                    Console.WriteLine("Printing received message.\n");
                    foreach (var message in receiveMessageResponse.Messages)
                    {
                        if (!string.IsNullOrEmpty(message.Body))
                        {
                            if (message.MessageId.Equals(messageID))
                            {
                                XmlDocument dom = new XmlDocument();

                                XmlElement messageTag = dom.CreateElement("Message");
                                dom.AppendChild(messageTag);

                                XmlElement MessageMD5 = dom.CreateElement("MessageMD5");
                                messageTag.AppendChild(MessageMD5);
                                XmlText textmd5 = dom.CreateTextNode(message.MD5OfBody);
                                MessageMD5.AppendChild(textmd5);
                                XmlElement MessageID = dom.CreateElement("Identifier");
                                messageTag.AppendChild(MessageMD5);
                                XmlText textID = dom.CreateTextNode(message.MessageId);
                                MessageID.AppendChild(textID);
                                XmlElement MessageString = dom.CreateElement("String");
                                messageTag.AppendChild(MessageID);
                                XmlText textString = dom.CreateTextNode(message.Body);
                                MessageString.AppendChild(textString);
                                // Console.WriteLine("Deleting the message.\n");
                                var messageRecieptHandle = message.ReceiptHandle;

                                var deleteRequest = new DeleteMessageRequest { QueueUrl = myQueueUrlWB, ReceiptHandle = messageRecieptHandle };
                                sqsWB.DeleteMessage(deleteRequest);
                                return dom;
                            }

                        }
                    }


                }
            }
            catch (AmazonSQSException ex)
            {
                Console.WriteLine("Caught Exception: " + ex.Message);
                Console.WriteLine("Response Status Code: " + ex.StatusCode);
                Console.WriteLine("Error Code: " + ex.ErrorCode);
                Console.WriteLine("Error Type: " + ex.ErrorType);
                Console.WriteLine("Request ID: " + ex.RequestId);
                XmlDocument dom = new XmlDocument();

                XmlElement messageTag = dom.CreateElement("Error");
                dom.AppendChild(messageTag);

                XmlElement MessageMD5 = dom.CreateElement("RequestID");
                messageTag.AppendChild(MessageMD5);
                XmlText textmd5 = dom.CreateTextNode(ex.RequestId);
                MessageMD5.AppendChild(textmd5);
                XmlElement MessageID = dom.CreateElement("Exception");
                messageTag.AppendChild(MessageID);
                XmlText textID = dom.CreateTextNode(ex.Message);
                MessageID.AppendChild(textID);
                return dom;
            }

            Console.WriteLine("Press Enter to continue...");
            Console.Read();
            XmlDocument dom2 = new XmlDocument();

            XmlElement messageTag2 = dom2.CreateElement("Error");
            dom2.AppendChild(messageTag2);

            XmlElement Message = dom2.CreateElement("Message");
            messageTag2.AppendChild(Message);
            XmlText textmd = dom2.CreateTextNode("Unable to locate the message with this identifier");
            Message.AppendChild(textmd);
            return dom2;


        }
    }
}
